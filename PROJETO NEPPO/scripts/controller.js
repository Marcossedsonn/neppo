

function Controller($scope, $http, $route) {

    var baseUrl = "https://test-frontend-neppo.herokuapp.com/pessoas/";
    $scope.pessoas = [];
    $scope.novaspessoas = [];
    $scope.pessoaSelecionada={};
    $scope.newPessoa ={};
    $scope.showPessoaDetail=false;
    $scope.showPessoalter=false;
    $scope.showPessoaDel=false;
    $scope.showPessoacadastra=false;


    function getListPessoas() {
        $scope.pessoas = [];
        $http.get(baseUrl)
        .success(function (data) {
            $scope.pessoas = data;
        })
        .error(function (error) {
            console.log("Retorno: " + error);
        });
    }

    getListPessoas();


     // get single record from database
     $scope.getidPessoas =  function(id) {
        
        $http.get(baseUrl+id)
        .success(function (data) {
            $scope.pessoaSelecionada = data;
            $scope.showPessoaDetail=true;
            console.log($scope.pessoaSelecionada);
        })
        .error(function (error) {
            console.log("Retorno: " + error);
        });
    };


    
    $scope.newPessoa ={};
    $scope.showNewPessoaFields = false;

    $scope.addPessoa = function() {

        
        $http.post(baseUrl, $scope.newPessoa )

        .success(function (data) {
            $scope.pessoaInserida = data;
            $scope.showNewPessoaFields = true;


            console.log("Inserido com sucesso");
            $scope.showNewPessoaFields = false;
            getListPessoas();

        })

        .error(function (error) {
            console.log("Retorno: " + error);
            
        });

        

    }



    $scope.getremovPessoas = function(id) {

        
        $http.delete(baseUrl+id)
        .success(function (data) {
            console.log("excluido com sucesso");
            getListPessoas();

        })
        .error(function (error) {
            console.log("Retorno: " + error);
        });
    }

    
    
    $scope.getalterPessoas=  function(id) {
        
        $http.get(baseUrl+id)
        .success(function (data) {
            $scope.pessoaSelecionada = data;
            $scope.showPessoalter=true;
            console.log($scope.pessoaSelecionada);
        })
        .error(function (error) {
            console.log("Retorno: " + error);
        });
    };




    
    

}


